export class Evento {
    nombre: string;
    lugar: string;
    fecha: string;


public constructor(nombre: string, lugar: string, fecha: string) {
    this.nombre = nombre;
    this.lugar = lugar;
    this.fecha = fecha;
}

public setNombre(nombre: string){
    this.nombre = nombre;
}
public getNombre(){
    return this.nombre;
}
public setLugar(lugar: string){
    this.lugar = lugar;
}
public getLugar(){
    return this.lugar;
}
public setFecha(fecha: string){
    this.fecha = fecha;
}
public getFecha(){
    return this.fecha;
}


}