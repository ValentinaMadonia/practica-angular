import { Component, OnInit  } from '@angular/core';
import { Evento } from '../modelos/evento';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {
  eventos: Evento[] = [];

  ngOnInit() {
    this.eventos.push(new Evento('Cosquín Rock', 'Aeroclub Santa María de Punilla', '02/10/2022'));
    this.eventos.push(new Evento('Arctic Monkeys en Argentina', 'Hipódromo de San Isidro', '03/30/2022'));
    this.eventos.push(new Evento('Lollapalooza Argentina', 'Hipódromo de San Isidro', '04/05/2022'));
    this.eventos.push(new Evento('Iron Maiden en Argentina', 'Estadio Vélez Sársfield', '10/12/2022'));
  }
}
