import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {
  
  nro1:number;
  nro2:number;
  resultado:number;
  sumar(){
    this.resultado=this.nro1+this.nro2;
  }
  restar(){
    this.resultado=this.nro1-this.nro2;
  }
  dividir(){
    this.resultado=this.nro1/this.nro2;
  }
  multiplicar(){
    this.resultado=this.nro1*this.nro2;
  }
  ngOnInit() { 
  }

}
